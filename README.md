# README #

### Overview ###
This module provides a tiled "Bento" search that allows you to integrate results
from multiple search backends into a single display. Each panel is loaded
asyncronously.

### Installation ###

Add the repository to your project composer.json as follows:
```
{
    "type": "vcs",
    "url": "git@bitbucket.org:mediacurrent/bento_search.git"
}
```

Then add the following to your installer_paths:
```
"web/modules/shared/{$name}": ["type:drupal-custom-module"],
```

This will make the bento_search module go in web/modules/shared/bento_search when you run composer install.

Then run:
```composer require mediacurrent/bento_search dev-master```

