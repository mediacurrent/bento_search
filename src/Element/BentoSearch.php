<?php

namespace Drupal\bento_search\Element;

use Drupal\Core\Render\Element\RenderElement;
use Drupal\Core\Url;

/**
 * Provides a render element to display a Bento Search layout.
 *
 * @RenderElement("bento_search")
 */
class BentoSearch extends RenderElement {
  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#pre_render' => [
        [$class, 'preRenderBentoSearchElement'],
      ],
      '#cache' => [],
    ];
  }

  /**
   * Bento Search element pre-render callback.
   */
  public static function preRenderBentoSearchElement($element) {
    $plugin_manager = \Drupal::service('plugin.manager.bento_search_panel');
    $definitions = $plugin_manager->getWeightedDefinitions();

    $query_string = \Drupal::request()->query->get('query');

    $build = [
      '#theme' => 'bento_panels_layout',
      '#attached' => [
        'library' => [
          'bento_search/async-panels',
        ],
        'drupalSettings' => [
          'bentoSearch' => [
            'queryString' => $query_string,
            'panels' => [],
          ]
        ],
      ],
    ];

    foreach ($definitions as $plugin_id => $definition) {
      if (!$definition['active']) {
        continue;
      }

      // Populate both the theme and Javascript variables.
      $build['#panels'][$plugin_id] = [
        'id' => $plugin_id,
        'title' => $definition['label'],
      ];
      $build['#attached']['drupalSettings']['bentoSearch']['panels'][$plugin_id] = [
        'pluginId' => $plugin_id,
        'url' => Url::fromRoute('bento_search.panel', ['name' => $plugin_id], ['absolute' => TRUE])->toString(),
      ];
    }

    return $build;
  }
}
