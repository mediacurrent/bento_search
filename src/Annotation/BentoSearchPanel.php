<?php

namespace Drupal\bento_search\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Bento search panel item annotation object.
 *
 * @see \Drupal\bento_search\Plugin\BentoSearchPanelManager
 * @see plugin_api
 *
 * @Annotation
 */
class BentoSearchPanel extends Plugin {


  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * Whether this Bento Panel is active.
   *
   * @var boolean
   */
  public $active;

  /**
   * The weight of this plugin which determines order in the default template.
   *
   * @var integer
   */
  public $weight;

}
