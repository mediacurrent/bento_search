<?php

namespace Drupal\bento_search\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class DefaultBentoSearchController.
 */
class DefaultBentoSearchController extends ControllerBase {

  public function getTitle() {
    return $this->t('Bento Search');
  }

  /**
   * Render method.
   *
   * @return array
   *   A render array.
   */
  public function render() {
    $search = [
      '#type' => 'bento_search',
    ];

    return [
      'search' => $search,
    ];
  }

}
