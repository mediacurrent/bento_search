<?php

namespace Drupal\bento_search\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class BentoPanelContentController.
 */
class BentoPanelContentController extends ControllerBase {

  /**
   * Render.
   *
   * @return string
   *   Return Hello string.
   */
  public function render(Request $request, $name) {
    $plugin_manager = \Drupal::service('plugin.manager.bento_search_panel');
    $bento_panel_instance = $plugin_manager->createInstance($name);

    if (!$bento_panel_instance) {
      throw new NotFoundHttpException();
    }

    $query_string = $request->query->get('query');

    if (!$query_string) {
      // @TODO Throw some kind of better exception.
      throw new NotFoundHttpException();
    }

    $bento_panel_instance->initSearch($query_string);

    $build = [
      '#theme' => 'bento_panel',
      '#cache' => [
        'max-age' => 0
      ],
      '#id' => $name,
      '#query_string' => $query_string,
    ];

    $html = \Drupal::service('renderer')->render($build);

    return new JsonResponse([
      'panelHtml' => $html,
      'count' => $bento_panel_instance->getCount(),
      'metadata' => $bento_panel_instance->getMetadata(),
    ]);
  }

}
