<?php

namespace Drupal\bento_search\Plugin;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for Bento search panel plugins.
 */
abstract class BentoSearchPanelBase extends PluginBase implements BentoSearchPanelInterface {

  /**
   * @var integer Count
   */
  private $searchData;

  /**
   * Execute a query against a backend and return the results.
   *
   * @param string $query_string
   *   A search string to pass to a search service.
   *
   * @return array
   *   Any relevant data needed to display results. The following keys are
   *   recognized:
   *   - results
   *   - count
   *   - metadata
   */
  abstract function loadSearch($query_string);

  /**
   * Returns the "View more results" URL for a given panel.
   *
   * @param string $query_string
   *   A search string to pass factor into the URL.
   *
   * @return string
   *   An absolute URL in string form.
   */
  abstract function getServiceUrl($query_string);

  /**
   * Initialize a search
   *
   * This method makes use of static caching to ensure that it only hits the
   * backend once even if it is called in different places. The following
   * methods only work after calling ::initSearch():
   * - getResults()
   * - getCount()
   * - getMetadata()
   *
   * @param $query_string
   *   A search string to pass to a search service.
   */
  public function initSearch($query_string) {
    $definition = $this->getPluginDefinition();
    $plugin_id = $definition['id'];

    $cache = &drupal_static('bento_search_panel_data');

    if (empty($cache[$plugin_id])) {
      $cache[$plugin_id] = $this->loadSearch($query_string);
    }

    $this->setSearchData($cache[$plugin_id]);
  }

  /**
   * Setter method for searchData
   *
   * @param array $data
   *   Data from a ::loadSearch call.
   */
  private function setSearchData(array $data) {
    $this->searchData = $data;
  }

  /**
   * Getter method for searchData
   *
   * @return array
   */
  public function getSearchData() {
    return $this->searchData;
  }

  /**
   * Extract results out of an initialized Bento search.
   *
   * Always run ::initSearch($query_string) before.
   *
   * @return array
   *   A list of search results
   */
  public function getResults() {
    return $this->getSearchData()['results'];
  }

  /**
   * Extract result count out of an initialized Bento search.
   *
   * Always run ::initSearch($query_string) before.
   *
   * @return integer
   *   The full result count of the search.
   */
  public function getCount() {
    return $this->getSearchData()['count'];
  }

  /**
   * Extract metadata out of an initialized Bento search.
   *
   * Always run ::initSearch($query_string) before. Metadata can be used in the
   * template as you please.
   *
   * @return array
   *   An associative array of metadata.
   */
  public function getMetadata() {
    return $this->getSearchData()['metadata'];
  }
}
