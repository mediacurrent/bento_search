<?php

namespace Drupal\bento_search\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Bento search panel plugins.
 */
interface BentoSearchPanelInterface extends PluginInspectionInterface {

  /**
   * Execute a query against a backend and return the results.
   *
   * @param string $query_string
   *   A search string to pass to a search service.
   *
   * @return array
   *   Any relevant data needed to display results.
   */
  public function loadSearch($query_string);

  /**
   * Returns the "View more results" URL for a given panel.
   *
   * @param string $query_string
   *   A search string to pass factor into the URL.
   *
   * @return string
   *   An absolute URL in string form.
   */
  public function getServiceUrl($query_string);
}
