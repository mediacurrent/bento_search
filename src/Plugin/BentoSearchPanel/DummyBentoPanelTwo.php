<?php

namespace Drupal\bento_search\Plugin\BentoSearchPanel;

use Drupal\bento_search\Plugin\BentoSearchPanelBase;

/**
 * Plugin for a dummy Bento Search Panel.
 *
 * @BentoSearchPanel(
 *   id = "dummy_two",
 *   label = @Translation("Dummy Panel Two"),
 *   active = FALSE,
 *   weight = 100
 * )
 */
class DummyBentoPanelTwo extends BentoSearchPanelBase {

  /**
   * {@inheritdoc}
   */
  public function loadSearch($query_string) {
    $data = [
      'results' => [
        [
          'title' => 'Result 4',
          'url' => 'https://www.google.com',
          'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        ],
        [
          'title' => 'Result 5',
          'url' => 'https://www.google.com',
          'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        ],
        [
          'title' => 'Result 6',
          'url' => 'https://www.google.com',
          'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        ],
      ],
      'count' => 100,
      'metadata' => [
        'metadata_2' => t('Metadata 2'),
      ],
    ];

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function getServiceUrl($query_string) {
    return 'https://www.google.com/search?q=' . $query_string;
  }
}
