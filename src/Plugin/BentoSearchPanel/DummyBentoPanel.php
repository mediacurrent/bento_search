<?php

namespace Drupal\bento_search\Plugin\BentoSearchPanel;

use Drupal\bento_search\Plugin\BentoSearchPanelBase;

/**
 * Plugin for a dummy Bento Search Panel.
 *
 * @BentoSearchPanel(
 *   id = "dummy",
 *   label = @Translation("Dummy Panel"),
 *   active = FALSE,
 *   weight = 50
 * )
 */
class DummyBentoPanel extends BentoSearchPanelBase {

  /**
   * {@inheritdoc}
   */
  public function loadSearch($query_string) {
    $data = [
      'results' => [
        [
          'title' => 'Result 1',
          'url' => 'https://www.google.com',
          'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        ],
        [
          'title' => 'Result 2',
          'url' => 'https://www.google.com',
          'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        ],
        [
          'title' => 'Result 3',
          'url' => 'https://www.google.com',
          'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        ],
      ],
      'count' => 100,
      'metadata' => [
        'metadata_1' => t('Metadata 1'),
      ],
    ];

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function getServiceUrl($query_string) {
    return 'https://www.google.com/search?q=' . $query_string;
  }
}
