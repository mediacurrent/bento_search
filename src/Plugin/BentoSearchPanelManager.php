<?php

namespace Drupal\bento_search\Plugin;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides the Bento search panel plugin manager.
 */
class BentoSearchPanelManager extends DefaultPluginManager {


  /**
   * Constructs a new BentoSearchPanelManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/BentoSearchPanel', $namespaces, $module_handler, 'Drupal\bento_search\Plugin\BentoSearchPanelInterface', 'Drupal\bento_search\Annotation\BentoSearchPanel');

    $this->alterInfo('bento_search_bento_search_panel_info');
    $this->setCacheBackend($cache_backend, 'bento_search_bento_search_panel_plugins');
  }

  /**
   * Get definitions sorted by weight.
   *
   * @return array|\mixed[]|null
   *   A list of plugin definitions.
   */
  public function getWeightedDefinitions() {
    $definitions = $this->getDefinitions();

    uasort($definitions, function ($a, $b) {
      if ($a['weight'] == $b['weight']) {
        return 0;
      }
      return ($a['weight'] < $b['weight']) ? -1 : 1;
    });

    return $definitions;
  }

}
