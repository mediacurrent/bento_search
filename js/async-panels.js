(function ($) {
  'use strict';

  Drupal.bentoSearch = Drupal.bentoSearch || {};
  var bentoSearch = Drupal.bentoSearch;

  bentoSearch.replacePanelHtml = function (pluginId, panelHtml) {
    var placeholder = $(".bento-panel-layout .bento-panel-wrapper[data-bento-plugin-name='" + pluginId + "']");

    if (!placeholder.length) {
      return;
    }

    placeholder.html(panelHtml)
  };

  bentoSearch.setCount = function (pluginId, count) {
    var currentPanelSummaryItem = $(".bento-result-summary li[data-bento-plugin-name='" + pluginId + "']");

    if (currentPanelSummaryItem.length) {
      var currentPanelCountWrapper = currentPanelSummaryItem.find('.count-text');
      currentPanelCountWrapper.html(count);
      currentPanelSummaryItem.css('display', 'block');
    }
  };

  $('.bento-panel-layout .bento-panel-wrapper').each(function() {
    var pluginId = $(this).attr('data-bento-plugin-name');
    if (typeof drupalSettings.bentoSearch.panels[pluginId].url === 'undefined') {
      return;
    }

    var panelSettings = drupalSettings.bentoSearch.panels[pluginId];
    var panelServiceUrl = panelSettings.url;

    $.ajax({
      url: panelServiceUrl,
      type: 'get',
      data: {
        query: drupalSettings.bentoSearch.queryString
      },
      success: function(data) {
        if (typeof data.panelHtml !== 'undefined') {
          // Set panel HTML.
          bentoSearch.replacePanelHtml(pluginId, data.panelHtml);
        }

        if (typeof data.count !== 'undefined') {
          bentoSearch.setCount(pluginId, data.count);
        }
      }
    });

  });

}(jQuery));
